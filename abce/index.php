<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<!-- jQuery -->

<title>ACB e-Leraring</title>


<div class="container contact">	
	<h2>Alumnos enrolados en curusos</h2>	
	<div class="table-responsive">	
		<div class="top-panel">
			<div class="btn-group pull-right">
				<button type="button" class="btn btn-primary btn-lg dropdown-toggle" data-toggle="dropdown">Descargar<span class="caret"></span></button>
				<ul class="dropdown-menu" role="menu">
					<li><a class="dataExport" data-type="csv">CSV</a></li>
					<li><a class="dataExport" data-type="excel">XLS</a></li>          
					<li><a class="dataExport" data-type="txt">TXT</a></li>
				</ul>
			</div>
		</div>	
		<table id="dataTable" class="table table-striped">
			<thead>
				<tr>
					<th>Nombre Usuario</th>
					<th>Nombre</th>
					<th>Apelildo</th>
					<th>Curso</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				include_once('./config.php');
    			include_once('./consulta.php');

			    try {
			       
			        $responseBody = consulta();	 
			       
			        for($i = 0; $i < count($responseBody) ; $i++) {			          
			          echo  "<tr>";
			          echo "<td>".$responseBody[$i]['username']."</td>";
			          echo "<td>".$responseBody[$i]['nombre']."</td>";
			          echo "<td>".$responseBody[$i]['apellido']."</td>";
			          echo "<td>".$responseBody[$i]['curso']."</td>";
			          echo "</tr>";
			    	
			      }
			        
			    } catch (Exception $e) {
			        // buscar que es sendInternalServerError
			        
				}
			    ?>
			</tbody>
		</table>	
	</div>
</div>	
<script src="tableExport/tableExport.js"></script>
<script type="text/javascript" src="tableExport/jquery.base64.js"></script>
<script src="js/export.js"></script>
