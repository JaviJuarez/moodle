<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'block_abce', language 'en', branch 'MOODLE_20_STABLE'
 *
 * @package   block_abce
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


$string['abce:addinstance'] = 'Add a new ABCE block';
$string['abce:myaddinstance'] = 'Add a new ABCE block to Dashboard';
$string['newhtmlblock'] = '(new abce block)';
$string['pluginname'] = 'abce';

